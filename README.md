# Experis Academy - NodeJs Console application

This is an console application in NodeJs for the Noroff periode of Experis Academy.

## Usage
Clone/fork repository.
Run `npm install` to get the dependencies.
To satrt the script, do `npm start`.
Follow the instruction in the terminal.

### Note
The functionality behind the users choice has been extracted to helper.js. This is make the code in index.js a bit more readable.
This also shows the usage of `module.export.xxx`
 
and `const { xxx } = require('helper.js');`, provided from Node Js.