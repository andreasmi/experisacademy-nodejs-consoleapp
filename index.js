const readline = require('readline');
const {readFile, displayOsInfo, startHttpServer} = require('./helper.js');

const hostname = 'localhost';
const port = 3000;

const optionsPrompt = `Choose an option:
1. Read package.json
2. Display OS info
3. Start HTTP server
`;
const numbers = /^[123]{1}$/;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log(optionsPrompt);
rl.question('Type a number: ', (answer) => {
    if(!answer.match(numbers)){
        console.log("Your input is invalid!")
        return;
    }
    console.log('\n');
    switch(answer){
        case '1':
            readFile(`${__dirname}\\package.json`, 'Reading package.json');
            break;
        case '2':
            displayOsInfo('Getting OS info...');
            break;
        case '3':
            startHttpServer(hostname, port, 'Starting HTTP server...');
            break;
    }
    rl.close();
});

