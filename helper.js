const fs = require('fs');
const os = require('os');
const http = require('http');


module.exports.readFile = (file, message = false) => {
    if(message){
        console.log(message);
    }
    fs.readFile(file, 'utf8', function(err, contents){
        if(err){
            console.log(`Something went wrong: \n${err}`);
            return;
        }
        console.log(contents);
    });
}

module.exports.displayOsInfo = (message = false) => {
    if(message){
        console.log(message);
    }
    const totalMem = (os.totalmem()/1024/1024/1024).toFixed(2);
    const freeMem = (os.freemem()/1024/1024/1024).toFixed(2);
    const cpuCores = os.cpus().length;
    const arch = os.arch();
    const platform = os.platform();
    const release = os.release();
    const user = os.userInfo().username;
    
    const output = `
    SYSTEM MEMORY: ${totalMem}
    FREE MEMORY: ${freeMem}
    CPU CORES: ${cpuCores}
    ARCH: ${arch}
    PLATFORM: ${platform}
    RELEASE: ${release}
    USER: ${user}`;

    console.log(output);
}

module.exports.startHttpServer = (hostname, port, message = false) => {
    if(message){
        console.log(message);
    }
    const server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hello World');
    });
    server.listen(port, hostname, () => {
        console.log(`Server running at http://${hostname}:${port}`);
    });
}